/*****************************************************************************
 * loop_bug_Core0.cpp
 *****************************************************************************/

#include <sys/platform.h>
#include <sys/adi_core.h>
#include <ccblkfn.h>
#include "adi_initialize.h"
#include "loop_bug_Core0.h"

#include <iostream>

/** 
 * If you want to use command program arguments, then place them in the following string. 
 */
char __argv_string[] = "";

void loop_fn(unsigned char* sOut, unsigned int len)
{
    unsigned char buf[10];

    buf[0] = 'a';
    buf[1] = 'b';
    buf[2] = 'c';

    int i, j;

    for(i=0, j=len-1; j>=0; j--, i++){
    	sOut[i] = buf[j];
    }
}

#pragma no_vectorization
void loop_fn_no_vectorize(unsigned char* sOut, unsigned int len)
{
    unsigned char buf[10];

    buf[0] = 'a';
    buf[1] = 'b';
    buf[2] = 'c';

    int i, j;

    for(i=0, j=len-1; j>=0; j--, i++){
    	sOut[i] = buf[j];
    }
}

int main(int argc, char *argv[])
{
	/**
	 * Initialize managed drivers and/or services that have been added to 
	 * the project.
	 * @return zero on success 
	 */
	adi_initComponents();
	
	/**
	 * The default startup code does not include any functionality to allow
	 * core 0 to enable core 1. A convenient way to enable
	 * core 1 is to use the adi_core_enable function. 
	 */
	adi_core_enable(ADI_CORE_1);

	/* Begin adding your custom code here */

	unsigned char buffer[3];

	loop_fn(buffer, 3);

	std::cout << "TEST(vectorized loop): " << std::hex << (int)buffer[0] << ", " << (int)buffer[1] << ", " << (int)buffer[2] << std::endl;

	loop_fn_no_vectorize(buffer, 3);

	std::cout << "TEST(non-vectorized loop): " << std::hex << (int)buffer[0] << ", " << (int)buffer[1] << ", " << (int)buffer[2] << std::endl;

	return 0;
}

